
/*
  Задание 1:
    Написать скрипт, который по клику на кнопку рандомит цвет и записывает его в localStorage
    После перезагрузки страницы, цвет должен сохранится.

  Задание 2:
    Написать форму логина (логин пароль), которая после отправки данных записывает их в localStorage.
    Если в localStorage есть запись - На страниче вывести "Привет {username}", если нет - вывести окно
    логина.
    + бонус, сделать кнопку "выйти" которая удаляет запись из localStorage и снова показывает форму логина
    + бонус сделать проверку логина и пароля на конкретную запись. Т.е. залогинит пользователя если
    он введет только admin@example.com и пароль 12345678
*/

var changeColor = document.getElementById('changeColor');
var mainForm = document.getElementById('mainForm');
var greeting = document.getElementById('greeting');
var exit = document.getElementById('exit');

changeColor.addEventListener('click', function () {
    if(!localStorage.backGroundColor){
        var color = randomColor();
        document.body.style.backgroundColor = color;
        localStorage.backGroundColor = color;
    }
    document.body.style.backgroundColor = localStorage.backGroundColor;
    function randomColor() {
        return "#" + Math.random().toString(16).slice(2, 8);
    };
});

mainForm.addEventListener('submit', function (event) {
    event.preventDefault();
    var name = mainForm.children[0];
    var password = mainForm.children[1];
    if(name.value.length < 3 || password.value.length < 1){
        alert('Too small name or password!');
        return;
    };
    if(localStorage[name.value] && localStorage[name.value] === password.value){
        greeting.innerHTML = 'Hello ' + name.value + '!';
    } else {
        localStorage[name.value] = password.value;
        alert('You have registered!');
    };

    name.value = '';
    password.value = '';
});

exit.addEventListener('click', function () {
    var name = mainForm.children[0];
    var password = mainForm.children[1];

    if(name.value.length < 3 || password.value.length < 1){
        alert('Too small name or password!');
        return;
    };
    if(localStorage[name.value] && localStorage[name.value] === password.value){
        localStorage.removeItem(name.value);
        greeting.innerHTML = 'User was removed!';
    } else {
        console.log(localStorage[name.value] === password.value);
        alert('No user with this name!');
    }
    name.value = '';
    password.value = '';
});

